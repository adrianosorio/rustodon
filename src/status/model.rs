use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct SourceImage {
    pub image_title: String,
    pub image_source_url: String,
    pub explanation: String,
    pub published_date:  String,
}

pub struct LocalImage {
    pub id: u64,
    pub file_path: String,
    pub url: String,
}

#[derive(Serialize, Deserialize)]
pub struct Status {
    pub status: String,
    pub media_ids: Vec<u64> 
}

pub enum CustomError {
    Io(std::io::Error),
    Reqwest(reqwest::Error),
    Serde(serde_json::error::Error),
}

impl From<std::io::Error> for CustomError {
    fn from(err: std::io::Error) -> CustomError {
        CustomError::Io(err)
    }
}

impl From<reqwest::Error> for CustomError {
    fn from(err: reqwest::Error) -> CustomError {
        CustomError::Reqwest(err)
    }
}

impl From<serde_json::error::Error> for CustomError {
    fn from(err: serde_json::error::Error) -> CustomError {
        CustomError::Serde(err)
    }
}
