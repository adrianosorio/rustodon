use crate::status::{CustomError, Status};
use reqwest::{Client};
use std::env;

pub fn publish(status: Status) -> Result<(), CustomError> {
    let api_url = "https://mstdn.mx/api/v1/statuses?access_token=";
    let token = env::var("TOKEN").expect("Access token not set");
    let dest = format!("{}{}", api_url, token);
    debug!("dest url: '{}'", dest);
 
    let client = Client::new();
    let result = client.post(&dest)
        .json(&status);

    debug!("Publish Result: {}", result.send()?.text()?);   
    Ok(())

}
