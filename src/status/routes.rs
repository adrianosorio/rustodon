use crate::status::{SourceImage, Status, get_image, post_image, publish};
use actix_web::{post, web, HttpResponse, Responder};
use serde_json::json;

#[post("/status")]
async fn create(source_image: web::Json<SourceImage>) -> impl Responder {
    info!("Image Src: {}", source_image.image_source_url);

    match get_image(&source_image.image_source_url) {
        Err(_error) => {            
            error!("An error was occured while downloading the image. ");
        },
        Ok(img) => {
            debug!("Donwload image competed. {}", img.file_path);
            let remote_image = post_image(img); 
            match remote_image {
                Err(_error) => {
                    error!("An error was occurred while uploading the image. ");
                },
                Ok(result) => {
                    debug!("Result ID: {}, URL: {}", result["id"], result["url"]);
                    let status_text = String::from(&source_image.image_title);
                    let img_id = result["id"].as_str().unwrap();

                    let status = Status {
                        status: status_text,
                        media_ids: vec![img_id.parse().unwrap()]
                    };
                    debug!("Status: status:{}, media_ids:{}", status.status, status.media_ids[0]);
                    let _publish = publish(status);
                }
            }
        }
    }

    let src_json = &source_image.into_inner();
    info!("{}", json!(src_json));

    HttpResponse::Created().json(src_json)
}

pub fn init_routes(cfg: &mut web::ServiceConfig) {
    //cfg.service(find_all);
    //cfg.service(find);
    //cfg.service(update);
    //cfg.service(delete);
    cfg.service(create);
}
